# Ansible Collection - artem_shestakov.docker_swarm

### How to
1. Create inventory
```
[first_master]
xxx.xxx.xxx.xxx

[swarm_masters]
xxx.xxx.xxx.xxx

[swarm_workers]
xxx.xxx.xxx.xxx
xxx.xxx.xxx.xxx
```
* [first_master] firs or only Master node
* [swarm_masters] another Master nodes
* [swarm_workers] Worker nodes  


2. Create playbook
3. Run playbook
